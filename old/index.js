const melee = require('./melee');
const ranged = require('./ranged');
const shields = require('./shields');
const mutations = require('./mutations');
const skills = require('./skills');

let slot1; //first slot
let slot2; //second slot
let skill1; //first skill
let skill2; //second skill
let mut1; //mutations
let mut2;
let mut3;

let err; //keep track of if something went wrong

const type = Math.floor((Math.random() * 2) + 1);
const stat = Math.floor((Math.random() * 3) + 1);
const sType =  Math.floor((Math.random() * 3) + 1);
const sType2 =  Math.floor((Math.random() * 3) + 1);


function pickRandom(x) {
  return x[Math.floor(Math.random() * x.length)];
}

//melee weapon
if (type == 1 && stat == 1) { slot1 = pickRandom(melee.brutalityMelee) };
if (type == 1 && stat == 2) { slot1 = pickRandom(melee.tacticsMelee) };
if (type == 1 && stat == 3) { slot1 = pickRandom(melee.survivalMelee) };

//ranged weapon
if (type == 2 && stat == 1) { slot1 = pickRandom(ranged.brutalityRanged) };
if (type == 2 && stat == 2) { slot1 = pickRandom(ranged.tacticsRanged) };
if (type == 2 && stat == 3) { slot1 = pickRandom(ranged.survivalRanged) };

slot2 = pickRandom(shields.Shields); //shield

//if statement chain of doom

//skill slot one if turret
if (sType == 1 && stat == 1) { skill1 = pickRandom(skills.brutalityTurrets) };
if (sType == 1 && stat == 2) { skill1 = pickRandom(skills.tacticsTurrets) };
if (sType == 1 && stat == 3) { skill1 = pickRandom(skills.survivalTurrets) };

//skill slot one if grenade
if (sType == 2 && stat == 1) { skill1 = pickRandom(skills.brutalityGrenades) };
if (sType == 2 && stat == 2) { skill1 = pickRandom(skills.tacticsGrenades) };
if (sType == 2 && stat == 3) { skill1 = pickRandom(skills.survivalGrenades) };

//skill slot one if power
if (sType == 3 && stat == 1) { skill1 = pickRandom(skills.brutalityPowers) };
if (sType == 3 && stat == 2) { skill1 = pickRandom(skills.tacticsPowers) };
if (sType == 3 && stat == 3) { skill1 = pickRandom(skills.survivalPowers) };

//ditto but 2 - turrets
if (sType2 == 1 && stat == 1) { skill2 = pickRandom(skills.brutalityTurrets) };
if (sType2 == 1 && stat == 2) { skill2 = pickRandom(skills.tacticsTurrets) };
if (sType2 == 1 && stat == 3) { skill2 = pickRandom(skills.survivalTurrets) };

//ditto but 2 - grenades
if (sType2 == 2 && stat == 1) { skill2 = pickRandom(skills.brutalityGrenades) };
if (sType2 == 2 && stat == 2) { skill2 = pickRandom(skills.tacticsGrenades) };
if (sType2 == 2 && stat == 3) { skill2 = pickRandom(skills.survivalPowers) };

//ditto but 2 - powers
if (sType2 == 3 && stat == 1) { skill2 = pickRandom(skills.brutalityPowers) };
if (sType2 == 3 && stat == 2) { skill2 = pickRandom(skills.tacticsPowers) };
if (sType2 == 3 && stat == 3) { skill2 = pickRandom(skills.survivalPowers) };

//do it all again with mutations aaaaaaaaaaaaaaaaaaaaaaaaaa
switch (type) {
  case 1:
    console.log("case 1");
    mut1 = pickRandom(mutations.meleeMutations);
    mut2 = pickRandom(mutations.meleeMutations);
    mut3 = pickRandom(mutations.miscMutations);
    break;
  case 2:
    console.log("case 2");
    mut1 = pickRandom(mutations.rangedMutations);
    mut2 = pickRandom(mutations.rangedMutations);
    mut3 = pickRandom(mutations.miscMutations);
    break;
  default:
    let err = 1;
    break;
}

if (err == 1) {
  console.log('Something went wrong while choosing mutations...');
}
else {
  console.log("-- your build is --")
  console.log(`Weapon: ${slot1}`);
  console.log(`Shield: ${slot2}`);
  console.log(`Skills: ${skill1} and ${skill2}`);
  console.log(`Mutations: ${mut1}, ${mut2}, and ${mut3}`);
};

console.log("Debug Info: ", type, stat, sType, sType2);


