const items = require("./items.js");

let stat;
let weapon1;
let weapon2;
let skill1;
let skill2;
let mut1;
let mut2;
let mut3;

function hasSameStat(item) {
    if(stat == 1) return item.stat == 1 || item.stat == 12 || item.stat == 13;
    if(stat == 2) return item.stat == 2 || item.stat == 12 || item.stat == 23;
    if(stat == 3) return item.stat == 3 || item.stat == 13 || item.stat == 23;
    return false;
}

function pickRandomFromStat(items) {
    let filteredItems = items.filter(hasSameStat);
    return filteredItems[Math.floor(Math.random() * filteredItems.length)];
}

//pick stat
stat = Math.floor(Math.random() * 3) + 1;

//pick weapons
weapon1 = pickRandomFromStat(items.weapons.filter((item) => !item.shield));
if(weapon1.support) { //needs a main weapon that synergizes
    let filteredItems = items.weapons.filter((item) => !(item.dual || item.shield || item.support));
    switch(weapon1.status) {
        case 1: //bleeding
            weapon2 = pickRandomFromStat(filteredItems.filter((item) => item.status == 1 || item.status == -1));
            break;
        case 2: //poison
            weapon2 = pickRandomFromStat(filteredItems.filter((item) => item.status == 2 || item.status == -1));
            break;
        case 3: //shock
            weapon2 = pickRandomFromStat(filteredItems.filter((item) => item.status == 3));
            break;
        case 4: //fire
            weapon2 = pickRandomFromStat(filteredItems.filter((item) => item.status == 4 && item.status != 5));
            break;
        case 5: //freeze
            weapon2 = pickRandomFromStat(filteredItems.filter((item) => (item.status == 5 || item.status == -2 || item.speed == 1) && item.status != 4));
            break;
        case 6: //root
            weapon2 = pickRandomFromStat(filteredItems.filter((item) => item.status == 6 || item.status == -2 || item.speed == 1));
            break;
        case 7: //slow
            weapon2 = pickRandomFromStat(filteredItems.filter((item) => item.speed == 1));
            break;
        case 8: //stun
            weapon2 = pickRandomFromStat(filteredItems.filter((item) => item.status == 8 || item.status == -2 || item.speed == 1));
            break;
        case -3: //hokuto
            weapon2 = pickRandomFromStat(filteredItems.filter((item) => item.status >= 1 && item.status <= 4));
            break;
    }
    if(weapon2 == null) weapon2 = pickRandomFromStat(filteredItems);

    //swap the weapons so the support is in secondary slot
    let weaponTemp = weapon1;
    weapon1 = weapon2;
    weapon2 = weaponTemp;
}
else if(!weapon1.dual) { //get this man a shield!
    let filteredItems = items.weapons.filter((item) => item.shield);
    if(weapon1.status > 0) weapon2 = pickRandomFromStat(filteredItems.filter((item) => item.status == weapon1.status));
    if(weapon1.status == -1) weapon2 = pickRandomFromStat(filteredItems.filter((item) => item.status == 1 || item.status == 2));
    if(weapon1.status == -2) weapon2 = pickRandomFromStat(filteredItems.filter((item) => item.status == 5 || item.status == 6 || item.status == 8));
    if(weapon2 == null) weapon2 = pickRandomFromStat(filteredItems);
}

//pick skills - TODO

//pick mutations - TODO

//print build
console.log("-- Your Build Is --");
if(!weapon1.dual) {
    console.log("Primary:", weapon1.name);
    console.log("Secondary:", weapon2.name);
}
else {
    console.log("Primary:", weapon1.name.split("|")[0]);
    console.log("Secondary:", weapon1.name.split("|")[1]);
}

if(stat == 1) console.log("Stat: Brutality");
if(stat == 2) console.log("Stat: Survival");
if(stat == 3) console.log("Stat: Tactics");