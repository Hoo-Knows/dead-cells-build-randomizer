const Shields = [
  "Old Wooden Shield",
  "Front Line Shield",
  "Cudgel",
  "Punishment",
  "Knockback Shield",
  "Rampart",
  "Bloodthirsty Shield",
  "Assault Shield",
  "Greed Shield",
  "Spiked Shield",
  "Parry Shield",
  "Force Shield",
  "Thunder Shield",
  "Ice Shield",
]

module.exports = { Shields };
