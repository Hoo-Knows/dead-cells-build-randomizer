function Weapon(name, stat, dual, shield, ranged, melee, ammo, longRange, speed, support, status) {

  this.name = name; // string: weapon's name
  this.stat = stat; // integer: weapon's color; 1 - brutality, 2 - survival, 3 - tactics, combine for dual scale (eg; iron staff would be 12)
  this.dual = dual; // boolean: is it two-handed?
  this.shield = shield; // boolean: is it a shield?
  this.ranged = ranged; // boolean: is it a ranged weapon?
  this.melee = melee; // boolean: is it a melee weapon?
  this.ammo = ammo; // boolean: is it affected by the ammo mutation?
  this.longRange = longRange; // boolean: is it a long-distance weapon (eg; nerves of steel or marksman's bow)
  this.speed = speed; // integer: how fast is it? 0 - shield, 1 - slow, 2 - medium, 3 - autofire
  this.support = support; // boolean: is it a support weapon? (eg; frost blast)   
  this.status = status; // integer: does this inflict a status or crit on a status?
                  // 0 = no status; 1 = bleed; 2 = poison; 3 = shock; 4 = fire; 5 = freeze; 6 = root; 7 = slow; 8 = stun
                  // -1 = this weapon is sadist's stiletto; -2 = this weapon is nutcracker, -3 = this weapon is hokuto's bow
  
};

var weapons = [];

//4482423842 generic melee weapons
weapons.push(new Weapon("Rusty Sword", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Balanced Blade", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Assassin's Dagger", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Spite Sword", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Blood Sword", 1, false, false, false, true, false, false, 2, false, 1));
weapons.push(new Weapon("Twin Daggers", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Broadsword", 2, false, false, false, true, false, false, 1, false, 0));
weapons.push(new Weapon("Shovel", 2, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Cursed Sword", 1, false, false, false, true, false, false, 3, false, 0));
weapons.push(new Weapon("Sadist's Stiletto", 13, false, false, false, true, false, false, 2, false, -1));
weapons.push(new Weapon("Swift Sword", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Giantkiller", 12, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Shrapnel Axes", 13, false, false, true, true, true, false, 2, false, 0));
weapons.push(new Weapon("Seismic Strike", 2, false, false, false, true, false, false, 1, true, 6));
weapons.push(new Weapon("War Spear", 12, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Impaler", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Symmetrical Lance", 2, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Rapier", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Meat Skewer", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Nutcracker", 2, false, false, false, true, false, false, 1, false, -2));
weapons.push(new Weapon("Spartan Sandals", 13, false, false, false, true, false, false, 2, true, 0));
weapons.push(new Weapon("Spiked Boots", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Hayabusa Boots", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Hayabusa Gauntlets", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Valmont's Whip", 13, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Wrenching Whip", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Oiled Sword", 1, false, false, false, true, false, false, 2, false, 4));
weapons.push(new Weapon("Torch", 1, false, false, false, true, false, false, 2, true, 4));
weapons.push(new Weapon("Frantic Sword", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Flawless", 12, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Flint", 12, false, false, false, true, false, false, 1, false, 4));
weapons.push(new Weapon("Tentacle", 13, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Vorpan", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Scythe Claw|Left Scythe Claw", 2, true, false, false, true, false, false, 1, false, 0));
weapons.push(new Weapon("Rhythm n' Bouzouki", 2, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Crowbar", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Snake Fangs", 13, false, false, false, true, false, false, 2, false, 2));
weapons.push(new Weapon("Iron Staff", 12, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Ferryman's Lantern|Soul Shot", 13, true, false, true, true, true, false, 2, false, 0));
weapons.push(new Weapon("Hattori's Katana", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Tombstone", 2, false, false, false, true, false, false, 1, false, 0));
weapons.push(new Weapon("Oven Axe", 2, false, false, false, true, false, false, 1, false, 0));
weapons.push(new Weapon("Toothpick", 2, false, false, false, true, false, false, 1, false, 8));
weapons.push(new Weapon("Machete and Pistol", 13, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Hard Light Sword|Hard Light Gun", 13, true, false, true, true, true, false, 2, false, 0));
weapons.push(new Weapon("Pure Nail", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Bone", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Abyssal Trident", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Hand Hook", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Maw of the Deep", 2, false, false, false, true, false, false, 1, false, 6));
weapons.push(new Weapon("Bladed Tonfas", 1, false, false, false, true, false, false, 2, false, 0));
weapons.push(new Weapon("Wrecking Ball", 2, false, false, false, true, false, false, 1, false, 0));
weapons.push(new Weapon("Queen's Rapier", 1, false, false, false, true, false, false, 2, false, 0));

//ranged weapons
weapons.push(new Weapon("Beginner's Bow", 3, false, false, true, false, true, false, 2, false, 0));
weapons.push(new Weapon("Multiple-Knocks Bow", 3, false, false, true, false, true, false, 2, false, 0));
weapons.push(new Weapon("Bow and Endless Quiver", 3, false, false, true, false, false, false, 2, false, 0));
weapons.push(new Weapon("Marksman's Bow", 3, false, false, true, false, true, true, 2, false, 0));
weapons.push(new Weapon("Sonic Carbine", 3, false, false, true, false, true, false, 3, false, 0));
weapons.push(new Weapon("Infantry Bow", 13, false, false, true, false, true, false, 2, false, 0));
weapons.push(new Weapon("Quick Bow", 3, false, false, true, false, true, false, 3, false, 0));
weapons.push(new Weapon("Ice Bow", 23, false, false, true, false, true, false, 2, true, 5));
weapons.push(new Weapon("Heavy Crossbow|Reload", 23, true, false, true, false, true, false, 1, false, 6));
weapons.push(new Weapon("Repeater Crossbow|Quiver of Bolts", 23, true, false, true, false, true, false, 3, false, 6));
weapons.push(new Weapon("Ice Crossbow|Piercing Shot", 23, true, false, true, false, true, false, 2, false, 5));
weapons.push(new Weapon("Explosive Crossbow|Cross Hit", 23, true, false, true, true, true, false, 1, false, 0));
weapons.push(new Weapon("Alchemic Carbine", 3, false, false, true, false, false, false, 2, true, 2));
weapons.push(new Weapon("Boomerang", 3, false, false, true, false, false, false, 2, true, 0));
weapons.push(new Weapon("Hemorrhage", 13, false, false, true, false, false, false, 1, false, 1));
weapons.push(new Weapon("The Boy's Axe", 23, false, false, true, false, true, false, 2, true, 6));
weapons.push(new Weapon("War Javelin", 3, false, false, true, false, false, false, 2, true, 0));
weapons.push(new Weapon("Hokuto's Bow", 3, false, false, true, false, false, false, 2, true, -3));
weapons.push(new Weapon("Nerves of Steel", 3, false, false, true, false, true, true, 2, false, 0));
weapons.push(new Weapon("Throwing Knife", 13, false, false, true, false, true, false, 2, true, 1));
weapons.push(new Weapon("Electric Whip", 3, false, false, true, false, false, false, 2, false, 3));
weapons.push(new Weapon("Firebrands", 13, false, false, true, false, false, false, 2, true, 4));
weapons.push(new Weapon("Ice Shards", 23, false, false, true, false, false, false, 3, true, 7));
weapons.push(new Weapon("Pyrotechnics", 13, false, false, true, false, false, false, 2, false, 4));
weapons.push(new Weapon("Lightning Bolt", 3, false, false, true, false, false, false, 2, false, 3));
weapons.push(new Weapon("Fire Blast", 13, false, false, true, false, false, false, 2, false, 4));
weapons.push(new Weapon("Frost Blast", 23, false, false, true, false, false, false, 1, true, 5));
weapons.push(new Weapon("Magic Missiles", 3, false, false, true, false, false, false, 2, false, 0));
weapons.push(new Weapon("Blowgun", 3, false, false, true, false, true, false, 2, false, 2));
weapons.push(new Weapon("Barrel Launcher", 3, false, false, true, false, false, false, 1, false, 0));
weapons.push(new Weapon("Killing Deck", 3, false, false, true, false, true, false, 1, false, 0));
weapons.push(new Weapon("Gilded Yumi", 3, false, false, true, false, false, false, 1, true, 0));

//shields
weapons.push(new Weapon("Old Wooden Shield", 2, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Front Line Shield", 12, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Cudgel", 2, false, true, false, false, false, false, 0, true, 8));
weapons.push(new Weapon("Punishment", 2, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Knockback Shield", 23, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Rampart", 2, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Bloodthirsty Shield", 12, false, true, false, false, false, false, 0, true, 1));
weapons.push(new Weapon("Assault Shield", 12, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Greed Shield", 2, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Spiked Shield", 2, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Parry Shield", 23, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Force Shield", 2, false, true, false, false, false, false, 0, true, 0));
weapons.push(new Weapon("Thunder Shield", 23, false, true, false, false, false, false, 0, true, 3));
weapons.push(new Weapon("Ice Shield", 2, false, true, false, false, false, false, 0, true, 5));

//skill issue
function Skill(name, stat, cd, status, pet, turret) {
  this.name = name; //string: the skill's name
  this.stat = stat; //integer: the stat, numbers to use are at the top of the file
  this.cd = cd; //integer: the skill's cooldown in seconds
  this.status = status; //integer: does this inflict a status? status numbers are at the top of the file
  this.pet = pet; //boolean: is this a pet
  this.turret = turret; //boolean: is this a turret
};




module.exports = { weapons };
