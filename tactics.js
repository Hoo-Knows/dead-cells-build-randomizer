const weaponsMelee = [
    "Valmont's Whip",
    "Tentacle",
    "Snake Fangs",
    "Infantry Bow",
    "The Boy's Axe",
];

const weaponsRanged = [
    "Beginner's Bow",
    "Multiple-Knocks Bow",
    "Bow and Endless Quiver",
    "Marksman's Bow",
    "Sonic Carbine",
    "Quick Bow",
    "Ice Bow",
    "Alchemic Carbine",
    "Boomerang",
    "Hemorrhage",
    "War Javelin",
    "Nerves of Steel",
    "Ice Shards",
    "Pyrotechnics",
    "Lightning Bolt",
    "Fire Blast",
    "Frost Blast",
    "Blowgun",
    "Magic Missiles",
    "Barrel Launcher",
    "Killing Deck",
    "Gilded Yumi",
]

const weaponsDualwield = [
    "Hard Light Sword",
    "Heavy Crossbow",
    "Repeater Crossbow",
    "Ice Crossbow",
    "Explosive Crossbow",
]

const weaponsDOT = [
    "Hokuto's Bow",
    "Throwing Knife",
    "Electric Whip",
    "Firebrands",
]

const shields = [
    "Knockback Shield",
    "Parry Shield",
    "Thunder Shield",
]

const skillsTurret = [
    "Double Crossb-o-matic",
    "Sinew Slicer",
    "Heavy Turret",
    "Barnacle",
    "Flamethrower Turret",
    "Cleaver",
    "Wolf Trap",
    "Crusher",
    "Explosive Decoy",
    "Emergency Door",
    "Tesla Coil",
    "Scavenged Bombard",
]

const skillsGrenade = [
    "Magentic Grenade",
    "Oil Grenade",
    "Swarm",
]

const skillsPower = [
    "Leghugger",
    "Pollo Power",
    "Cocoon",
    "Scarecrow Sickes",
    "Lightning Rods",
    "Smoke Bomb",
    "Lightspeed",
    "Great Owl of War",
    "Wings of the Crow",
    "Wave of Denial",
    "Corrosive Cloud",
    "Phaser",
    "Tornado",
    "Knife Dance",
    "Corrupted Power",
]

const mutationsGeneric = [
    "Acrobatipack",
    "Ranger's Gear",
    "Hunter's Instinct",
    "Ammo",
]

const mutationsMelee = [
    "Point Blank",
    "Ripper",
]

const mutationsRanged = [
    "Tranquility",
    "Parting Gift",
]

const mutationsDOT = [
    "Barbed Tips",
    "Networking",
]

const mutationsMisc = [
    "Extended Healing",
    "Gastronomy",
    "Ygdar Orus Li Ox",
    "Recovery",
    "Emergency Triage",
    "Velocity",
    "Alienation",
    "Acceptance",
    "Masochist",
    "Dead Inside",
    "Disengagement",
    "No Mercy",
    "Instinct of the Master of Arms",
    "Armadillopack",
]

const weaponsAll = weaponsMelee.concat(weaponsRanged.concat(weaponsDualwield.concat(weaponsDOT)));
const mutationsAll = mutationsGeneric.concat(mutationsMelee.concat(mutationsRanged.concat(mutationsDOT)));

module.exports = {
    weaponsMelee, weaponsRanged, weaponsDualwield, weaponsDOT, weaponsAll, shields,
    skillsTurret, skillsGrenade, skillsPower,
    mutationsGeneric, mutationsMelee, mutationsRanged, mutationsDOT, mutationsAll, mutationsMisc
};